FROM node:16.14.0
ENV HOST 0.0.0.0
WORKDIR /user/src/app
COPY . .

RUN npm install

EXPOSE 9528

CMD [ "npm", "run", "dev" ]