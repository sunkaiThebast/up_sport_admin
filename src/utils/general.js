export function  myFormatDate(dd) {
  let now = new Date(dd)
  let year = now.getFullYear()
  let month = now.getMonth() + 1
  month = month < 10 ? '0' + month : month
  let date = now.getDate() < 10 ? '0' + now.getDate() : now.getDate()
  let hour = now.getHours() < 10 ? '0' + now.getHours() : now.getHours()
  let minute = now.getMinutes() < 10 ? '0' + now.getMinutes() : now.getMinutes()
  return year + '-' + month + '-' + date + ' ' + hour + ':' + minute + ':' + '00'
}