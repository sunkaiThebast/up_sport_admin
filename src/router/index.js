import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**

 * hidden:   如果设置为true，则项目不会显示在侧边栏中（默认为false）
 * redirect: 当设置 noRedirect 的时候该路由在面包屑导航中不可被点击
 * alwaysShow: true 如果设置为 true，将始终显示根菜单,如果不设置 alwaysShow，当 item 有多个子路由时，会变成嵌套模式，否则不显示根菜单
 * name:'router-name' <keep-alive> 使用的名称（必须设置！！！）
 * redirect: noRedirect 如果设置 noRedirect 将不会在面包屑中重定向
 * meta：{
    roles: ['admin','editor'] 控制页面角色（可以设置多个角色）
    title: 'title' 侧边栏和面包屑中显示的名称（推荐设置）
      noCache: true // 如果设置为true，则不会被 <keep-alive> 缓存(默认 false)
    icon: 'svg-name'/'el-icon-x' 侧边栏中显示的图标
    breadcrumb: false 如果设置为 false，该项目将隐藏在面包屑中（默认为 true）
    activeMenu: '/example/list' 如果设置路径，侧边栏会高亮你设置的路径
  }
 */

/**
 * 常量路由
 * 没有权限要求的基本页面
 * 所有角色都可以访问
 */
export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

  {
    path: '/',
    component: Layout,
    redirect: '/notice',
    children: [{
      path: 'notice',
      name: 'Notice',
      component: () => import('@/views/notice/index'),
      meta: { title: '公告', icon: 'dashboard' }
    }]
  },


  {
    path: '/account',
    component: Layout,
    redirect: '/account/personalsite',
    name: 'Account',
    meta: { title: '帳號', icon: 'el-icon-s-help' },
    children: [
      {
        path: 'personalsite',
        name: 'Personalsite',
        component: () => import('@/views/account/personalsite'),
        meta: { title: '個人設置', icon: 'el-icon-user' }
      },
      {
        path: 'sub',
        name: 'Sub',
        component: () => import('@/views/account/sub'),
        meta: { title: '子帳號管理', icon: 'el-icon-files' }
      },
      {
        path: 'online',
        name: 'Online',
        component: () => import('@/views/account/online'),
        meta: { title: '線上會員', icon: 'el-icon-s-platform' }
      }
    ]
  },
  {
    path: '/level',
    component: Layout,
    redirect: '/level/tree',
    meta: { title: '層級管理', icon: 'tree' },
    children: [
      {
        path: 'tree',
        name: 'Tree',
        redirect: '/level/tree/treebody',
        hidden:true,
        component: () => import('@/views/level/tree'),
        children:[
          {
            path: 'treebody',
            name: 'Treebody',
            hidden:true,
            component: () => import('@/views/level/treebody'),
            meta: { title: '层级数据' },
          },
          {
            path: 'updata',
            name: 'Updata',
            hidden:true,
            component: () => import('@/views/level/updata'),
            meta: { title: '修改数据' },
          },

        ]
      },

    ]
  },
  {
    path: '/report',
    component: Layout,
    redirect: '/report/instantbetting',
    name: 'Account',
    meta: { title: '查詢報表', icon: 'el-icon-s-data' },
    children: [
      {
        path: 'instantbetting',
        name: 'Instantbetting',
        component: () => import('@/views/report/instantbetting'),
        meta: { title: '即時注單', icon: 'table' }
      },
      {
        path: 'generalledger',
        name: 'Generalledger',
        component: () => import('@/views/report/generalledger'),
        meta: { title: '總帳報表', icon: 'el-icon-document' }
      },
      {
        path: 'change',
        name: 'Change',
        component: () => import('@/views/report/change'),
        meta: { title: '帳變紀錄', icon: 'el-icon-notebook-1' }
      }
    ]
  },
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
