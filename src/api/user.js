import request from '@/utils/request'

export function login(data) {
  return request({
    url: '/user/login',
    method: 'post',
    data
  })
}

export function getInfo(token) {
  return request({
    url: '/user/info',
    method: 'get',
    params: { token }
  })
}

export function logout() {
  return request({
    url: '/user/logout',
    method: 'post'
  })
}
// 线上会员
export function getOnline() {
  return request({
    url: '/user/online/list',
    method: 'get',
  })
}
// 剔除会员
export function getToEliminate(data) {
  return request({
    url: '/user/kick',
    method: 'post',
    data
  })
}
// 树桩图资料
export function getToTree() {
  return request({
    url: '/user/tree',
    method: 'get'
  })
}
