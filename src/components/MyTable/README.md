## my-table使用 说明
由于此项目内大多数表格都相似，此组件封装了el-table和el-pagination。只要写配置即可生成表格


## 示例

```html
<my-table
  @selection-change="tableSelectChange"
  border :data="tableData"
  :columns="columns"
  @paging-current-change="handleCurrentChange"
  :current-page="currentPage4"
  :page-size="10"
  :total="100">
</my-table>
```

## prop说明

#### columns
  列属性,要求是一个数组

  1. label: 显示在表头的文字
  2. prop: 对应data的key。Table将显示相应的value
  3. width: 每列的宽度，为一个数字(可选)
  4. type: 列的类型 (selection/index/expand)
  5. formatter: 格式化函数 (Function(row, column, cellValue, index))
  6. render : 自定义列渲染函数 (Function(h, { row }))  推荐写 jsx 如下示例
  
  其他需要的字段请参照 Table-column Attributes 自己添加
  
  如果你想要每个字段都有自定义的样式或者嵌套其他组件，columns可不提供，直接像在el-table一样写即可，如果没有自定义内容，提供columns将更加的便捷方便
  
  如果你有几个字段是需要自定义的，几个不需要，那么可以将不需要自定义的字段放入columns，将需要自定义的内容放入到slot中，详情见后文
  ```javascript
  [{
    prop:string,
    label:string,
    width:number,
    type:string,
    formatter:Function,
    
  },{
    prop:string,
    label:string,
    width:number,
    type:string,
    formatter:Function,
    render: (h, { row }) => {
      return (
        <el-button type='danger' on-click={() => this.del(row)}>删除</el-button>
      )
    }
  }]
  ```
  
#### paging
  是否开启分页  默认开启
  ```javascript
  {
    paging:Boolean,
    default:true
  }
    
  ```
#### paging
  是否开启table分页组件 默认开启  
 
#### 其他prop

表格和分页其他参数和事件请参照el文档。都可使用

## 事件

关于事件，el-table的相关事件，都可以查看el-table直接使用，

分页的事件，（由于table事件和paging事件冲突）目前只手动提供了一个页面变化时候的事件，有额外需要自己根据element-ui相应添加

@paging-current-change="handleCurrentChange" 返回参数值为当前选中的value

## 完整demo

```html
<template>
  <div class="my-table-demo">
    <my-table
      border
      ref="myTable"
      :data="tableData"
      highlight-current-row
      @current-change="handleTableCurrentChange"
      @selection-change="handleSelectionChange"
      :columns="columns"
      @paging-current-change="handlePagingCurrentChange"
      :current-page="queryForm.page"
      :page-size="queryForm.size"
      :total="tableTotal"/>
    <el-button type="primary" @click="test">让第三行勾选切换</el-button>
  </div>
</template>

<template>
  <div class="my-table-demo">
    <my-table
      border
      ref="myTable"
      :data="tableData"
      highlight-current-row
      @current-change="handleTableCurrentChange"
      @selection-change="handleSelectionChange"
      :columns="columns"
      @paging-current-change="handlePagingCurrentChange"
      :current-page="queryForm.page"
      :page-size="queryForm.size"
      :total="tableTotal"/>
    <el-button type="primary" @click="test">让第三行勾选切换</el-button>
  </div>
</template>

<script>
  import MyTable from '@/components/MyTable'

  export default {
    name: 'myTableDemo',
    components: { MyTable },
    data () {
      return {
        tableData: [{
          id: 1,
          date: '2016-05-02',
          name: '王小虎',
          address: '上海市普陀区金沙江路 1518 弄'
        }, {
          id: 2,
          date: '2016-05-04',
          name: '王小虎',
          address: '上海市普陀区金沙江路 1517 弄',
        }, {
          id: 3,
          date: '2016-05-01',
          name: '王小虎',
          address: '上海市普陀区金沙江路 1519 弄',
        }],
        columns: [
          {
            type: 'selection',
            selectable: (row, index) => {
              return row.id !== 2
            }
          },
          { label: '序号', type: 'index' },
          {
            prop: 'date',
            label: '日期',
            render: (h, { row }) => {
              const html = row.id === 2 ? row.date : row.date + ' 00:00:00'
              return (html)
            }
          },
          {
            prop: 'name',
            label: '姓名',
            render: (h, { row }) => {
              return (
                <el-popover trigger="hover" placement="top">
                  <p>姓名: {row.name}</p>
                  <p>住址: {row.address}</p>
                  <div slot="reference" className="name-wrapper">
                    <el-tag size="medium">{row.name}</el-tag>
                  </div>
                </el-popover>
              )
            }
          },
          { prop: 'address', label: '地址' },
          {
            label: '操作',
            fixed: 'right',
            render: (h, { row }) => {
              return (
                <span>
                  <el-button size="mini" type='success' on-click={() => this.del(row)}>添加</el-button>
                  <el-button size="mini" type='danger' on-click={() => this.del(row)}>删除</el-button>
                </span>
              )
            }
          }
        ],
        multipleSelection: [],
        queryForm: {
          page: 1,
          size: 10
        },
        tableTotal: 50
      }
    },
    methods: {
      del (row) {
        this.$message(`删除id：${row.id}`)
      },
      handleTableCurrentChange (currentRow, oldCurrentRow) {
        this.$message(`高亮行id：${currentRow.id}`)
      },
      handleSelectionChange (val) {
        this.multipleSelection = val
        console.log('复选框选中的', this.multipleSelection)
      },
      test () {
        console.log(this.$refs.myTable.$refs.elTable.toggleRowSelection(this.tableData[2]))
      },
      handlePagingCurrentChange (val) {
        this.$message(`分页页码：${val}`)
      }
    }
  }
</script>

```


 

